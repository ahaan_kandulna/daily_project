package com.sapient.oops;

import com.sapient.oops.payroll.Confirmed;
import com.sapient.oops.payroll.Employee;
import com.sapient.oops.payroll.Finance;
import com.sapient.oops.payroll.HR;
import com.sapient.oops.payroll.Intern;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        HR hr = new HR();
        Finance finance = new Finance();
        Employee employee =null;
        
        employee = hr.recruit("i");  //Upcast
        Finance.processPay(employee);
        employee = hr.recruit("p");  //Upcast
        Finance.processPay(employee);
        employee = hr.recruit("c");  //Upcast
//        Finance.processPay(employee);
        finance.processPay(employee);
        }
    }

