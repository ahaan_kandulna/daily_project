package com.sapient.oops.payroll;

public class Finance {

	public static void processPay(Employee employee) {
		if (employee!=null) {
			employee.netPay();
			if (employee instanceof Confirmed) {
				Confirmed confirmedEmployee = (Confirmed)employee;  //Downcast
				confirmedEmployee.transportation();
		}

	}

}}
