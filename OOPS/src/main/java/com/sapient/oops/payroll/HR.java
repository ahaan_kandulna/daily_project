package com.sapient.oops.payroll;

public class HR {
	public Employee recruit(String empType) {
		if (empType.equalsIgnoreCase("I")) {
			return new Intern();
		} else if (empType.equalsIgnoreCase("P")) {
			return new Confirmed();
		} else if (empType.equalsIgnoreCase("c")) {
			return new Contract();
		}
		return null;
	}
}
