package com.sapient.interfaces;

public interface SupermanFlyer {
	void leapBuilding();
	void stopBullet();
}
