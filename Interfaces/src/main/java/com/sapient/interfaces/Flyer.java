package com.sapient.interfaces;

public interface Flyer {
	void takeOff();
	void fly();
	void land();
	
}
