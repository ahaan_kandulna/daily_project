package com.sapient.interfaces;

public interface BirdFlyer {
	void buildNest();
	void layEggs();
}
