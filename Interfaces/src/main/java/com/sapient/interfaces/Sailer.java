package com.sapient.interfaces;

public interface Sailer {
	void dock();
	void cruise();
}
