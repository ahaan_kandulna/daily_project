package com.sapient.classobjects;

import com.sapient.classobjects.mamals.Man;

/**
 * Hello world!
 * 
 */

class Base{
	Base(){
		
	}
	
	Base(int i){
		
	}
	public void test1() {
		System.out.println("Test 1");
	}
	
	public void test2() {
		System.out.println("test 2");
	}
}

public class App 
{
    public static void main( String[] args )
    {
        Base b=new Base();
        b.test1();
        b.test2();
        
        new Base(3).test2();
    }
}
