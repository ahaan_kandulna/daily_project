package com.sapient.basics;

import com.sapient.oops.Polymorphism;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Polymorphism polymorphism = new Polymorphism();
        polymorphism.testing();
        System.out.println("App class");
    }
}
